﻿using System;
using System.Linq;
using UnityEngine;

namespace Alt3d
{
    public class AppView : MonoBehaviour
    {
        [SerializeField] private Transform ShortInfoWidgetsContainer;

        [SerializeField] private GameObject ShorInfoWidgetPrefab;

        [Space]
        [SerializeField] private FullWeatherInfoWidget FullInfoWidget;

        [SerializeField] private AddCityWidget AddCityWidget;

        public event Action<string> CityAddRequest;

        public event Action<long> CityDeleteRequest;

        private WeatherModel Model => App.Model;

        private void Awake()
        {
            AddCityWidget.Init(cityName => CityAddRequest?.Invoke(cityName));
        }

        private void OnEnable()
        {
            Model.Changed += OnModelChanged;
        }

        private void OnDisable()
        {
            Model.Changed -= OnModelChanged;
        }

        private void OnModelChanged()
        {
            Redraw();

            var last = Model.Items.LastOrDefault();
            if (last != null)
            {
                OnWidgetSelect(last);
            }
        }

        private void Redraw()
        {
            foreach (Transform child in ShortInfoWidgetsContainer)
            {
                Destroy(child.gameObject);
            }

            foreach (var item in Model.Items)
            {
                var go = Instantiate(ShorInfoWidgetPrefab, ShortInfoWidgetsContainer, false);

                var widget = go.GetComponent<ShortWeatherInfoWidget>();
                widget.Init(item, OnWidgetSelect, OnWidgetDelete);
            }
        }

        private void OnWidgetSelect(WeatherItem item)
        {
            FullInfoWidget.WeatherItem = item;
        }

        private void OnWidgetDelete(WeatherItem item)
        {
            CityDeleteRequest?.Invoke(item.Id);
        }
    }
}
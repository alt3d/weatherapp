﻿using System;
using Object = UnityEngine.Object;

namespace Alt3d
{
    public static class App
    {
        private static WeatherApi _api;

        public static WeatherApi API => _api ? _api : _api = Object.FindObjectOfType<WeatherApi>();

        private static WeatherStorage _storage;

        public static WeatherStorage Storage => _storage ? _storage : _storage = Object.FindObjectOfType<WeatherStorage>();

        private static WeatherModel _model;

        public static WeatherModel Model => _model ?? (_model = new WeatherModel());

        private static bool _isOfflineMode;

        public static bool IsOfflineMode
        {
            get => _isOfflineMode;
            set
            {
                _isOfflineMode = value;
                OfflineModeChanged?.Invoke(_isOfflineMode);
            }
        }

        public static event Action<bool> OfflineModeChanged;
    }
}
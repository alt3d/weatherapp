﻿using System.Collections.Generic;
using UnityEngine;

namespace Alt3d
{
    public class ClientSettings : ScriptableObject
    {
        [SerializeField] private float _refreshTime;

        [SerializeField] private string[] _defaultCities;

        public float RefreshTime => _refreshTime;

        public IEnumerable<string> DefaultCities => _defaultCities;
    }
}
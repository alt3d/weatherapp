﻿using System;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace Alt3d
{
    public class FullWeatherInfoWidget : MonoBehaviour
    {
        [SerializeField] private Text CityNameText;

        [SerializeField] private Text WeatherDescriptionText;

        [SerializeField] private Text TemperatureText;

        [Space]
        [SerializeField] private Text CloudinessText;

        [SerializeField] private Text HumidityText;

        [SerializeField] private Text WindSpeedText;

        [SerializeField] private Text PressureText;

        [Space]
        [SerializeField] private Image BackImage;

        [SerializeField] private Color NormalColor;

        [SerializeField] private Color OfflineColor;

        private WeatherItem _weatherItem;

        public WeatherItem WeatherItem
        {
            get => _weatherItem;
            set
            {
                _weatherItem = value;
                Redraw();
            }
        }

        private void OnEnable()
        {
            App.OfflineModeChanged += AppOnOfflineModeChanged;
        }

        private void OnDisable()
        {
            App.OfflineModeChanged -= AppOnOfflineModeChanged;
        }

        private void AppOnOfflineModeChanged(bool isOffline)
        {
            Redraw();
        }

        private void Redraw()
        {
            if (WeatherItem == null)
            {
                return;
            }
            
            CityNameText.text = WeatherItem.Name;
            WeatherDescriptionText.text = FirstCharToUpper(WeatherItem.Weather[0].Description);
            TemperatureText.text = $"{(int) WeatherItem.Main.Temp}°";

            CloudinessText.text = $"Облачность: {(int) WeatherItem.Clouds.All} %";
            HumidityText.text = $"Влажность: {WeatherItem.Main.Humidity} %";
            WindSpeedText.text = $"Ветер: {(int) WeatherItem.Wind.Speed} м/с";
            PressureText.text = $"Давление: {(int) (WeatherItem.Main.Pressure * 0.750062f)} мм рт. ст.";

            BackImage.color = App.IsOfflineMode ? OfflineColor : NormalColor;
        }

        private static string FirstCharToUpper(string value)
        {
            if (string.IsNullOrEmpty(value))
                throw new ArgumentNullException(nameof(value));

            return value.First().ToString().ToUpper() + value.Substring(1);
        }
    }
}
﻿using System;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace Alt3d
{
    public class AddCityWidget : MonoBehaviour
    {
        [SerializeField] private InputField AddCityInputField;

        [SerializeField] private Button AddCityButton;

        [FormerlySerializedAs("OnlineModeContainer")]
        [SerializeField] private GameObject RootContainer;

        private Action<string> CityAddAction { get; set; }

        private void OnEnable()
        {
            App.OfflineModeChanged += AppOnOfflineModeChanged;
            AddCityButton.onClick.AddListener(OnAddCityButtonClick);
        }

        private void OnDisable()
        {
            App.OfflineModeChanged -= AppOnOfflineModeChanged;
            AddCityButton.onClick.RemoveListener(OnAddCityButtonClick);
        }

        private void AppOnOfflineModeChanged(bool isOffline)
        {
            RootContainer.SetActive(!isOffline);
        }

        public void Init(Action<string> cityAddAction)
        {
            CityAddAction = cityAddAction;
        }

        private void OnAddCityButtonClick()
        {
            CityAddAction?.Invoke(AddCityInputField.text);
            AddCityInputField.text = string.Empty;
        }
    }
}
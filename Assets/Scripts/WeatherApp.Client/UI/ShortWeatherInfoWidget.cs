﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Alt3d
{
    public class ShortWeatherInfoWidget : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        [SerializeField] private Text CityNameText;

        [SerializeField] private Text TemperatureText;

        [SerializeField] private Text UpdateTimeText;

        [SerializeField] private Button SelectButton;

        [SerializeField] private Button DeleteButton;

        [SerializeField] private GameObject DeleteButtonContainer;

        private WeatherItem Item { get; set; }

        private Action<WeatherItem> SelectAction { get; set; }

        private Action<WeatherItem> DeleteAction { get; set; }

        private void OnEnable()
        {
            SelectButton.onClick.AddListener(OnSelectButtonClick);
            DeleteButton.onClick.AddListener(OnDeleteButtonClick);

            App.OfflineModeChanged += AppOnOfflineModeChanged;
        }

        private void OnDisable()
        {
            SelectButton.onClick.RemoveListener(OnSelectButtonClick);
            DeleteButton.onClick.RemoveListener(OnDeleteButtonClick);

            App.OfflineModeChanged -= AppOnOfflineModeChanged;
        }

        public void Init(WeatherItem item, Action<WeatherItem> selectAction, Action<WeatherItem> deleteAction)
        {
            Item = item;
            SelectAction = selectAction;
            DeleteAction = deleteAction;

            Redraw();
        }

        private void AppOnOfflineModeChanged(bool isOfflineMode)
        {
            Redraw();
        }

        private void Redraw()
        {
            if (Item == null)
            {
                return;
            }
            
            CityNameText.text = Item.Name;
            TemperatureText.text = $"{(int) Item.Main.Temp}°";

            UpdateTimeText.gameObject.SetActive(App.IsOfflineMode);
            if (App.IsOfflineMode)
            {
                var localDateTime = TimeZoneInfo.ConvertTimeFromUtc(Item.UpdateTime, TimeZoneInfo.Local);
                UpdateTimeText.text = $"Обновлено: {localDateTime:dd.MM.yy} в {localDateTime:HH:mm:ss}";
            }
        }

        private void OnSelectButtonClick()
        {
            SelectAction?.Invoke(Item);
        }

        private void OnDeleteButtonClick()
        {
            DeleteAction?.Invoke(Item);
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            DeleteButtonContainer.SetActive(true);
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            DeleteButtonContainer.SetActive(false);
        }
    }
}
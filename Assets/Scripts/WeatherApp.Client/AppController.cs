﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Alt3d
{
    public class AppController : MonoBehaviour
    {
        [SerializeField] private ClientSettings _settings;

        [SerializeField] private AppView _view;

        private ClientSettings Settings => _settings;

        private WeatherApi Api => App.API;

        private WeatherStorage Storage => App.Storage;

        private WeatherModel Model => App.Model;

        private AppView View => _view;

        private void OnEnable()
        {
            View.CityAddRequest += OnCityAddRequest;
            View.CityDeleteRequest += OnCityDeleteRequest;

            StartCoroutine(AutoRefresh());
        }

        private void OnDisable()
        {
            View.CityAddRequest -= OnCityAddRequest;
            View.CityDeleteRequest -= OnCityDeleteRequest;

            StopAllCoroutines();
        }

        private void Start()
        {
            var savedData = Storage.Load();
            if (savedData != null)
            {
                Model.SetState(savedData);

                var cityNames = Model.Items.Select(x => x.Name);
                UpdateModel(cityNames);
            }
            else
            {
                UpdateModel(Settings.DefaultCities);
            }
        }

        private IEnumerator AutoRefresh()
        {
            while (enabled)
            {
                yield return new WaitForSeconds(Settings.RefreshTime);
                var cityNames = Model.Items.Select(x => x.Name);
                UpdateModel(cityNames);
            }
        }

        private void UpdateModel(IEnumerable<string> cityNames)
        {
            foreach (var cityName in cityNames)
            {
                UpdateModel(cityName);
            }
        }

        private void UpdateModel(string cityName)
        {
            void onSuccess(WeatherItem item)
            {
                App.IsOfflineMode = false;
                Model.Add(item);

                var dto = Model.GetState();
                Storage.Save(dto);
            }

            void onFail()
            {
                App.IsOfflineMode = true;
            }

            Api.GetWeather(cityName, onSuccess, onFail);
        }

        private void OnCityAddRequest(string cityName)
        {
            if (!Model.Contains(cityName))
            {
                UpdateModel(cityName);
            }
        }

        private void OnCityDeleteRequest(long cityId)
        {
            var item = Model.Get(cityId);
            Model.Remove(item);

            var dto = Model.GetState();
            Storage.Save(dto);
        }
    }
}
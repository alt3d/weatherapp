﻿using UnityEngine;

namespace Alt3d
{
    public abstract class WeatherStorage : MonoBehaviour
    {
        public abstract void Save(WeatherDto data);

        public abstract WeatherDto Load();
    }
}
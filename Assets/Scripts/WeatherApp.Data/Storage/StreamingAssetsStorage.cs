﻿using System.IO;
using Newtonsoft.Json;
using UnityEngine;

namespace Alt3d
{
    internal class StreamingAssetsStorage : WeatherStorage
    {
        [SerializeField] private string _fileName;

        private string FileName => _fileName;

        public override void Save(WeatherDto data)
        {
            var path = GetFilePath();
            var json = JsonConvert.SerializeObject(data);
            File.WriteAllText(path, json);
        }

        public override WeatherDto Load()
        {
            var path = GetFilePath();
            if (!File.Exists(path))
            {
                return null;
            }

            var json = File.ReadAllText(path);
            return JsonConvert.DeserializeObject<WeatherDto>(json);
        }

        private string GetFilePath()
        {
            var path = Application.streamingAssetsPath;
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            return Path.Combine(path, FileName);
        }
    }
}
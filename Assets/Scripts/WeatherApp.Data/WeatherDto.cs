﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Alt3d
{
    [Serializable]
    public class WeatherDto
    {
        [JsonProperty("items")]
        public List<WeatherItem> Items = new List<WeatherItem>();
    }
}
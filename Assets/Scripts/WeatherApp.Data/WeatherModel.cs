﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Alt3d
{
    public class WeatherModel
    {
        private readonly List<WeatherItem> Collection;

        public IEnumerable<WeatherItem> Items => Collection;

        public event Action Changed;

        public bool Contains(long cityId)
        {
            return Collection.Any(x => x.Id == cityId);
        }

        public bool Contains(string cityName)
        {
            return Collection.Any(x => x.Name == cityName);
        }

        public WeatherItem Get(long cityId)
        {
            return Collection.FirstOrDefault(x => x.Id == cityId);
        }

        public WeatherItem Get(string cityName)
        {
            return Collection.FirstOrDefault(x => x.Name == cityName);
        }

        public void Add(WeatherItem item)
        {
            if (item == null)
                return;

            if (Collection.Contains(item))
                return;

            Collection.Add(item);
            Changed?.Invoke();
        }

        public void Update(WeatherItem item)
        {
            if (item == null)
                return;

            if (Collection.Contains(item))
                Collection.Remove(item);

            Collection.Add(item);
            Changed?.Invoke();
        }

        public void Remove(WeatherItem item)
        {
            if (item == null)
                return;

            if (!Collection.Contains(item))
                return;

            Collection.Remove(item);
            Changed?.Invoke();
        }

        public void SetState(WeatherDto dto)
        {
            Collection.Clear();

            foreach (var item in dto.Items)
            {
                Collection.Add(item);
            }

            Changed?.Invoke();
        }

        public WeatherDto GetState()
        {
            return new WeatherDto()
            {
                Items = new List<WeatherItem>(Collection)
            };
        }

        public WeatherModel()
        {
            Collection = new List<WeatherItem>();
        }
    }
}
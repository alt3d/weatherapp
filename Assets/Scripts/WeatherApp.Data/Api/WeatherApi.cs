﻿using System;
using UnityEngine;

namespace Alt3d
{
    public abstract class WeatherApi : MonoBehaviour
    {
        public abstract void GetWeather(long cityId, Action<WeatherItem> onSuccess, Action onFail = null);

        public abstract void GetWeather(string cityName, Action<WeatherItem> onSuccess, Action onFail = null);
    }
}
﻿using System;
using System.Collections;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.Networking;

namespace Alt3d
{
    internal class OpenWeatherMapApi : WeatherApi
    {
        [SerializeField] private string _apiKey;

        [SerializeField] private string _lang;

        [SerializeField] private string _units;

        private string ApiKey => _apiKey;

        private string Lang => _lang;

        private string Units => _units;

        protected void OnDestroy()
        {
            StopAllCoroutines();
        }

        public override void GetWeather(long cityId, Action<WeatherItem> onSuccess, Action onFail = null)
        {
            var uri = $"http://api.openweathermap.org/data/2.5/weather?id={cityId}&APPID={ApiKey}&lang={Lang}&units={Units}";
            StartCoroutine(SendRequest(uri, onSuccess, onFail));
        }

        public override void GetWeather(string cityName, Action<WeatherItem> onSuccess, Action onFail = null)
        {
            var uri = $"http://api.openweathermap.org/data/2.5/weather?q={cityName}&APPID={ApiKey}&lang={Lang}&units={Units}";
            StartCoroutine(SendRequest(uri, onSuccess, onFail));
        }

        private static IEnumerator SendRequest(string uri, Action<WeatherItem> onSuccess, Action onFail = null)
        {
            using (var request = UnityWebRequest.Get(uri))
            {
                yield return request.SendWebRequest();
                while (!request.isDone)
                {
                    yield return null;
                }

                if (request.responseCode == 200)
                {
                    var json = request.downloadHandler.text;
                    var item = JsonConvert.DeserializeObject<WeatherItem>(json);
                    item.UpdateTime = DateTime.UtcNow;
                    onSuccess?.Invoke(item);
                }
                else if (request.responseCode == 0)
                {
                    Debug.LogWarning("No internet connection");
                    onFail?.Invoke();
                }
                else
                {
                    Debug.LogWarning($"Request error. Code: {request.responseCode}");
                    Debug.LogWarning($"Uri: {uri}");
                    Debug.LogWarning($"Response: {request.downloadHandler.text}");
                    onFail?.Invoke();
                }
            }
        }
    }
}
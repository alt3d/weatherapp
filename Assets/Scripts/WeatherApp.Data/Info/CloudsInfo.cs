﻿using System;
using Newtonsoft.Json;

namespace Alt3d
{
    [Serializable]
    public class CloudsInfo
    {
        [JsonProperty("all")]
        public long All { get; set; }
    }
}
﻿using System;
using Newtonsoft.Json;

namespace Alt3d
{
    [Serializable]
    public class WindInfo
    {
        [JsonProperty("speed")]
        public double Speed { get; set; }

        [JsonProperty("deg")]
        public long Deg { get; set; }
    }
}